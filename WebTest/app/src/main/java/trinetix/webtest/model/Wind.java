package trinetix.webtest.model;

import com.google.gson.annotations.Expose;

/**
 * Created by mykola on 22.12.14.
 */
public class Wind {
    @Expose
    private Double speed;
    @Expose
    private Long deg;

    /**
     * @return The speed
     */
    public Double getSpeed() {
        return speed;
    }

    /**
     * @param speed The speed
     */
    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    /**
     * @return The deg
     */
    public Long getDeg() {
        return deg;
    }

    /**
     * @param deg The deg
     */
    public void setDeg(Long deg) {
        this.deg = deg;
    }
}
