package trinetix.webtest.model;

import com.google.gson.annotations.Expose;

/**
 * Created by mykola on 22.12.14.
 */
public class Sys {
    @Expose
    private Long type;
    @Expose
    private Long id;
    @Expose
    private Double message;
    @Expose
    private String country;
    @Expose
    private Long sunrise;
    @Expose
    private Long sunset;

    /**
     *
     * @return
     * The type
     */
    public Long getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(Long type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The message
     */
    public Double getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(Double message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The country
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     * The sunrise
     */
    public Long getSunrise() {
        return sunrise;
    }

    /**
     *
     * @param sunrise
     * The sunrise
     */
    public void setSunrise(Long sunrise) {
        this.sunrise = sunrise;
    }

    /**
     *
     * @return
     * The sunset
     */
    public Long getSunset() {
        return sunset;
    }

    /**
     *
     * @param sunset
     * The sunset
     */
    public void setSunset(Long sunset) {
        this.sunset = sunset;
    }


}
