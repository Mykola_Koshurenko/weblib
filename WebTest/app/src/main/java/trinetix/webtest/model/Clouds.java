package trinetix.webtest.model;

import com.google.gson.annotations.Expose;

/**
 * Created by mykola on 22.12.14.
 */
public class Clouds {

    @Expose
    private Long all;

    /**
     *
     * @return
     * The all
     */
    public Long getAll() {
        return all;
    }

    /**
     *
     * @param all
     * The all
     */
    public void setAll(Long all) {
        this.all = all;
    }

}