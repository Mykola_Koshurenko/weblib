package trinetix.webtest.service;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by mykola on 19.12.14.
 */
public interface ApiMethods {

    @GET("/data/2.5/weather")
    public WeatherResponse loadWeather(@Query("q") String p);
}
