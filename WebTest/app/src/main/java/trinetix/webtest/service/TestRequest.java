package trinetix.webtest.service;

import com.google.gson.Gson;
import com.trinetix.webrobo.request.BaseRequest;

/**
 * Created by mykola on 22.12.14.
 */
public abstract class TestRequest<T> extends BaseRequest<T, ApiMethods> {

    protected String serializedParams;

    protected Gson gson = new Gson();

    public TestRequest(Class<T> clazz) {
        super(clazz, ApiMethods.class);
    }

    /**
     * Creating a params string that will be used when executing the request.
     *
     * @param arg - the object to serialize.
     */
    public void setParams(Object arg) {
        serializedParams = gson.toJson(arg);
    }

    @Override
    public String getKey() {
        return "";
    }
}
