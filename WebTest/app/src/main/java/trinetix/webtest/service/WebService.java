package trinetix.webtest.service;

import android.app.Application;

import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.retrofit.GsonRetrofitObjectPersisterFactory;
import com.trinetix.webrobo.retrofit.RetrofitHelper;
import com.trinetix.webrobo.service.BaseRetrofitWebService;

import retrofit.RestAdapter;

/**
 * Created by mykola on 19.12.14.
 */
public class WebService extends BaseRetrofitWebService {

    public static final String END_POINT = "http://api.openweathermap.org";

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(ApiMethods.class);
    }

    @Override
    protected RestAdapter.Builder createDefaultRestAdapterBuilder() {
        RestAdapter.Builder builder = new RestAdapter.Builder();
        builder.setEndpoint(END_POINT);
        builder = RetrofitHelper.createBaseRestAdapterBuilder(builder, getApplicationContext());
        return builder;
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        GsonRetrofitObjectPersisterFactory persisterFactory = new GsonRetrofitObjectPersisterFactory(application);
        cacheManager.addPersister(persisterFactory);
        return cacheManager;
    }
}
