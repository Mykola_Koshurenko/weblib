package trinetix.webtest.service;

import com.trinetix.webrobo.response.BaseResponse;

import trinetix.webtest.model.Weather;

/**
 * Created by mykola on 22.12.14.
 */
public class WeatherResponse extends BaseResponse {

    private Weather weatherData;


    public Weather getWeather() {
        return weatherData;
    }

    public void setWeather(Weather weather) {
        this.weatherData = weather;
    }
}
