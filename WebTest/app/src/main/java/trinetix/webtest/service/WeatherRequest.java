package trinetix.webtest.service;

/**
 * Created by mykola on 22.12.14.
 */
public class WeatherRequest extends TestRequest<WeatherResponse> {
    private String p;

    public WeatherRequest(String param) {
        super(WeatherResponse.class);
        p = param;
    }

    @Override
    public String getKey() {
        return "eff";
    }

    @Override
    public WeatherResponse loadDataFromNetwork() {
        return getService().loadWeather(p);
    }
}
