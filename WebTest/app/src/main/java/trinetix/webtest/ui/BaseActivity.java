package trinetix.webtest.ui;


import com.octo.android.robospice.SpiceManager;
import com.trinetix.webrobo.controllers.BaseRequestController;
import com.trinetix.webrobo.ui.BaseAbstractActivity;

import trinetix.webtest.service.WebService;


/**
 * Created by mykola on 19.12.14.
 */
public abstract class BaseActivity extends BaseAbstractActivity {


    @Override
    protected SpiceManager createSpiceManager() {
        if (null == mSpiceManager)
            mSpiceManager = new SpiceManager(WebService.class);
        return mSpiceManager;
    }

    @Override
    public BaseRequestController createRequestController(SpiceManager spiceManager) {
        return null;
    }
}
