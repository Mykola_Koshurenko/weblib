package trinetix.webtest.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.trinetix.webrobo.controllers.BaseRequestController;
import com.trinetix.webrobo.controllers.CustomRequestListener;
import com.trinetix.webrobo.exceptions.WebServerError;
import com.trinetix.webrobo.request.Request;

import trinetix.webtest.R;
import trinetix.webtest.service.WeatherRequest;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void loadData(Bundle bundle, boolean isFirst) {
        WeatherRequest spicesRequest = new WeatherRequest("London,uk");
        Request request = new Request.RequestBuilder().request(spicesRequest).time(DurationInMillis.ALWAYS_EXPIRED).requestListener(new CustomRequestListener() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {

            }

            @Override
            public void onRequestSuccess(Object o) {
                Toast.makeText(MainActivity.this,"Ok",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRequestServerError(WebServerError error) {

            }

            @Override
            public void onInternetErrorConnection() {

            }
        }).build();
        getRequestController().exec(request, this);
    }

    @Override
    protected void initViews(boolean isFirst) {

    }

    @Override
    public BaseRequestController createRequestController(SpiceManager spiceManager) {
        return null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
