package com.trinetix.webrobo.ui;

import com.octo.android.robospice.SpiceManager;

/**
 * Created by mykola on 25.09.14.
 */
public interface ISpiceGetter {
    public SpiceManager getSpiceManager();
}
