package com.trinetix.webrobo.util;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by mykola on 08.12.14.
 */
public class Utils {
    /**
     * Check internet connections
     *
     * @param context context for get getSystemService
     * @return true if internet connection work
     */
    public static boolean isOnline(Context context) {
        if (null == context) {
            return false;
        }
        ConnectivityManager connectionManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == connectionManager) {
            return false;
        }
        return connectionManager.getActiveNetworkInfo() != null
                && connectionManager.getActiveNetworkInfo().isConnected();
    }
}
