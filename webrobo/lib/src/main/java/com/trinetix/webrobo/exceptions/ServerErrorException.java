package com.trinetix.webrobo.exceptions;

/**
 * Created by NK on 25.06.14.
 */
public class ServerErrorException extends Exception {

    private String errorCode;

    private String errorDetails;

    private int httpCode;

    public ServerErrorException() {
        super();
    }

    public ServerErrorException(String errorCode, String errorDetails, int httpCode) {
        this.errorCode = errorCode;
        this.errorDetails = errorDetails;
        this.httpCode = httpCode;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorDetails() {
        return errorDetails;
    }
}