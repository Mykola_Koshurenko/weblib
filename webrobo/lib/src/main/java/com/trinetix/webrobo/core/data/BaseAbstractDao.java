package com.trinetix.webrobo.core.data;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.DatabaseTableConfig;

import java.sql.SQLException;

/**
 * Created by mykola on 26.09.14.
 */
public class BaseAbstractDao<T, ID> extends BaseDaoImpl {
    protected BaseAbstractDao(Class dataClass) throws SQLException {
        super(dataClass);
    }

    protected BaseAbstractDao(ConnectionSource connectionSource, Class dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    protected BaseAbstractDao(ConnectionSource connectionSource, DatabaseTableConfig tableConfig) throws SQLException {
        super(connectionSource, tableConfig);
    }
}
