package com.trinetix.webrobo.core.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by mykola on 26.09.14.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DataHelper {

    String dataBaseName() default "db.db";

    int version() default 1;
}
