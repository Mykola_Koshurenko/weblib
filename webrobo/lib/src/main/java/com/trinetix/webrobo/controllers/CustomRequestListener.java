package com.trinetix.webrobo.controllers;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.trinetix.webrobo.exceptions.WebServerError;

/**
 * Created by mykola on 08.12.14.
 */
public interface CustomRequestListener<RESULT> {
    void onRequestFailure(SpiceException spiceException);

    void onRequestSuccess(RESULT result);

    void onRequestServerError(WebServerError error);

    void onInternetErrorConnection();
}
