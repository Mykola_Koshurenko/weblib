package com.trinetix.webrobo.controllers;

/**
 * Created by mykola on 25.09.14.
 */
public interface IBaseController {

    void clearRequest();
    void clearCache();
}
