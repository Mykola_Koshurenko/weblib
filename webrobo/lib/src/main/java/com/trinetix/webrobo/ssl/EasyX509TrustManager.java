package com.trinetix.webrobo.ssl;

import java.security.cert.CertificateException;

import javax.net.ssl.X509TrustManager;

/**
 * Custom X509 trust manager to trust everything.
 * <p/>
 * Created by fess on 27.11.14.
 */
public class EasyX509TrustManager implements X509TrustManager {

    @Override
    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
    }

    @Override
    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
    }

    @Override
    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}
