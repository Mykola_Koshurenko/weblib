package com.trinetix.webrobo;

/**
 * Created by mykola on 25.09.14.
 */
public abstract class RunnableExecutor implements Runnable {

    @Override
    public void run() {
        try {
            runWithThrow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public abstract void runWithThrow() throws Exception;
}
