package com.trinetix.webrobo.request;


import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.trinetix.webrobo.policy.NetworkRetryPolicy;
import com.trinetix.webrobo.response.BaseResponse;

/**
 * Created by NK on 25.06.14.
 */
public abstract class BaseRequest<T, R> extends RetrofitSpiceRequest<T, R> {
    public BaseRequest(Class<T> clazz, Class<R> retrofitedInterfaceClass) {
        super(clazz, retrofitedInterfaceClass);
        this.setRetryPolicy(new NetworkRetryPolicy());
    }

    public abstract String getKey();
}