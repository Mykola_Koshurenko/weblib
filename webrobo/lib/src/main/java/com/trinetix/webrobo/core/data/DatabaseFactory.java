package com.trinetix.webrobo.core.data;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by mykola on 26.09.14.
 */
public class DatabaseFactory {

    private ConcurrentHashMap<String, DatabaseHelper> mHelpers;
    private volatile static DatabaseFactory sFactory;

    {
        mHelpers = new ConcurrentHashMap();
    }

    public static synchronized DatabaseFactory getFactory() {
        if (sFactory == null) {
            sFactory = new DatabaseFactory();
        }
        return sFactory;
    }

    public synchronized DatabaseHelper getHelper(String key) {
        //use Database name for key
        return mHelpers.get(key);
    }

    public void putHelper(String key, DatabaseHelper helper) {
        mHelpers.put(key, helper);
    }


    public synchronized void releaseHelpers() {
        for (DatabaseHelper helper : mHelpers.values()) {
            helper.releaseHelper();
            helper.close();
        }
    }
}
