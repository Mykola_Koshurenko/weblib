package com.trinetix.webrobo.response;

import com.trinetix.webrobo.exceptions.WebServerError;

/**
 * Created by mykola on 08.12.14.
 */
public class BaseResponse {
    private WebServerError error;

    public WebServerError getError() {
        return error;
    }

    public void setError(WebServerError error) {
        this.error = error;
    }
}
