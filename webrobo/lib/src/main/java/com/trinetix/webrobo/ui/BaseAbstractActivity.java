package com.trinetix.webrobo.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.octo.android.robospice.SpiceManager;
import com.trinetix.webrobo.controllers.BaseRequestController;

/**
 * Created by mykola on 25.09.14.
 */
public abstract class BaseAbstractActivity extends FragmentActivity implements ISpiceGetter {

    protected SpiceManager mSpiceManager = null;

    protected BaseRequestController mRequestController;

    private boolean isFirst;


    @Override
    protected void onResume() {
        super.onResume();

        initViews(isFirst);
    }

    protected abstract SpiceManager createSpiceManager();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null == mSpiceManager) {
            mSpiceManager = createSpiceManager();
            if (null == mSpiceManager) throw new NullPointerException("Spice Manager is null");
        }

        if (null == mRequestController) {
            mRequestController = createRequestController(mSpiceManager);
            if (null == mRequestController) {
                mRequestController = new BaseRequestController(mSpiceManager);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mSpiceManager != null && !mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
        // Request request = new Request.RequestBuilder().request(null).requestListener(null).time(0).build();
        // mRequestController.exec(request, getSpiceManager(), this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (null != mRequestController) {
            mRequestController.onStop();
        }

        if (null != mSpiceManager && mSpiceManager.isStarted()) {
            mSpiceManager.shouldStop();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        isFirst = null == savedInstanceState;
        loadData(savedInstanceState, isFirst);
    }

    protected abstract void loadData(Bundle bundle, boolean isFirst);

    protected abstract void initViews(boolean isFirst);

    public abstract BaseRequestController createRequestController(SpiceManager spiceManager);

    @Override
    public SpiceManager getSpiceManager() {
        return mSpiceManager;
    }


    public BaseRequestController getRequestController() {
        return mRequestController;
    }

}
