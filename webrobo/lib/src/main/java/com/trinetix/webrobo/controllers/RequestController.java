package com.trinetix.webrobo.controllers;

import com.trinetix.webrobo.request.Request;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by mykola on 08.12.14.
 */
public class RequestController {
    protected Map<String, Request> requestMap;
    protected ConcurrentLinkedQueue<Request> requestsErrorStack;
    {
        requestMap = Collections.synchronizedMap(new HashMap<String, Request>());
        requestsErrorStack = new ConcurrentLinkedQueue();
    }

    /*public <T extends BaseResponse> Request initRequest(final BaseRequest request, final CustomRequestListener<T> result, long time) {
         Request requestItem = new Request.RequestBuilder().request(request).time(time).requestListener(result).build();
         requestMap.put(request.getKey(), requestItem);
         return requestItem;
     }*/

    public Request initRequest(Request request) {
        requestMap.put(request.getRequest().getKey(), request);
        return request;
    }

    public void closeRequests() {
        if (null != requestMap) {
            for (Request request : requestMap.values()) {
                request.getRequest().cancel();
            }
            clearRequest();
        }
    }

    public boolean remove(String key) {
        Request request = requestMap.remove(key);
        if (null == request) return false;
        return true;
    }

    public boolean closeRequest(String key) {
        Request request = requestMap.get(key);
        if (null != request && null != request.getRequest()) {
            request.getRequest().cancel();
            remove(request.getRequest().getKey());
            return true;
        }
        return false;
    }

    public Request getErrorRequest() {
        synchronized (requestsErrorStack) {
            if (requestsErrorStack.isEmpty()) return null;
            return requestsErrorStack.peek();
        }
    }

    public void putError(Request request) {
        synchronized (requestsErrorStack) {
            requestsErrorStack.add(request);
        }
    }

    public void clearErrorStack() {
        synchronized (requestsErrorStack) {
            requestsErrorStack.clear();
        }
    }


    private void clearRequest() {
        requestMap.clear();
    }
}
