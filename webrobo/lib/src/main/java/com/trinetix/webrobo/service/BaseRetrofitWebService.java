package com.trinetix.webrobo.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

import java.util.HashMap;
import java.util.Map;

import retrofit.RestAdapter;

/**
 * Created by NK on 25.06.14.
 */
public abstract class BaseRetrofitWebService extends RetrofitGsonSpiceService {
    protected Map<Class<?>, RestAdapter> mRetrofitInterfaceToRestAdapter;
    protected Map<Class<?>, Object> mRetrofitInterfaceToServiceMap = new HashMap<Class<?>, Object>();

    protected RestAdapter.Builder mDefaultRestAdapterBuilder;
    protected RestAdapter mDefaultRestAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected String getServerUrl() {
        //because we create RestAdapter ourselves
        return null;
    }

    /**
     * Fixing a crash, see below.
     *
     * @return notification.
     */
    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public Notification createDefaultNotification() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return null;
        } else {
            Notification notification = new Notification();

            notification.icon = getApplicationInfo().icon;
            //temporary fix https://github.com/octo-online/robospice/issues/200
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 0);
            notification.setLatestEventInfo(this, "", "", pendingIntent);

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                notification.priority = Notification.PRIORITY_MIN;
            }
            return notification;
        }
    }

    /**
     * Create default restAdapter
     *
     * @return rest adapter
     */
    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RestAdapter.Builder builder = createDefaultRestAdapterBuilder();
        mDefaultRestAdapterBuilder = builder;
        mDefaultRestAdapter = builder.build();
        return builder;
    }

    protected abstract RestAdapter.Builder createDefaultRestAdapterBuilder();


    protected void addRetrofitInterface(Class<?> serviceClass) {
        addRetrofitInterface(serviceClass, mDefaultRestAdapter);
    }


    protected void addRetrofitInterface(Class<?> serviceClass, RestAdapter restAdapter) {
        if (mRetrofitInterfaceToRestAdapter == null) {
            mRetrofitInterfaceToRestAdapter = new HashMap();
        }
        mRetrofitInterfaceToRestAdapter.put(serviceClass, restAdapter);
    }

    @Override
    protected <T> T getRetrofitService(Class<T> serviceClass) {
        T service = (T) mRetrofitInterfaceToServiceMap.get(serviceClass);
        if (service == null) {
            service = createRetrofitService(serviceClass);
            mRetrofitInterfaceToServiceMap.put(serviceClass, service);
        }
        return service;
    }

    protected <T> T createRetrofitService(Class<T> serviceClass) {
        if (mRetrofitInterfaceToRestAdapter != null && mRetrofitInterfaceToRestAdapter.containsKey(serviceClass)) {
            RestAdapter restAdapter = mRetrofitInterfaceToRestAdapter.get(serviceClass);
            return restAdapter.create(serviceClass);
        } else {
            return mDefaultRestAdapter.create(serviceClass);
        }
    }
}
