package com.trinetix.webrobo.request;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.trinetix.webrobo.controllers.CustomRequestListener;

/**
 * Created by mykola on 08.12.14.
 */
public class Request {
    private final BaseRequest request;
    private final long time;
    private final CustomRequestListener listener;


    private Request(BaseRequest request, long time, CustomRequestListener listener) {
        this.request = request;
        this.time = time;
        this.listener = listener;
    }

    public BaseRequest getRequest() {
        return request;
    }

    public long getTime() {
        return time;
    }

    public CustomRequestListener getListener() {
        return listener;
    }


    public static class RequestBuilder {
        private BaseRequest request;
        private long time;
        private CustomRequestListener listener;

        public RequestBuilder() {
            super();
        }

        public RequestBuilder request(BaseRequest request) throws NullPointerException {
            this.request = request;
            if (null == this.request) {
                throw new NullPointerException("Request is null");
            }
            return this;
        }

        public RequestBuilder time(long time) {
            if (time == 0) {
                time = DurationInMillis.ALWAYS_EXPIRED;
            }
            this.time = time;
            return this;
        }

        public RequestBuilder requestListener(CustomRequestListener listener) {
            if (null == listener) {
                throw new NullPointerException("Request listener");
            }
            this.listener = listener;
            return this;
        }

        public Request build() {
            if (null == request) throw new NullPointerException("Request is null");
            if (null == listener) throw new NullPointerException("Listener is null");
            return new Request(request, time, listener);
        }
    }
}