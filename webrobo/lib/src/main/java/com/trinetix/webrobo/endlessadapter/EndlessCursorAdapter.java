package com.trinetix.webrobo.endlessadapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Endless cursor adapter.
 * Created by NK on 11.07.14.
 */
public abstract class EndlessCursorAdapter extends CursorAdapter {

    private OnEndlessLoaderListener onEndlessLoaderListener=null;

    private Context mContext;
    private AtomicBoolean mKeepOnAppending;
    private View mPendingView;
    private AtomicBoolean mIsRunInBackground;
    private ExecutorService mExecutorService = null;//Executors.newSingleThreadExecutor();

    public EndlessCursorAdapter(Context context, Cursor c, boolean autoRequery, boolean keepOnAppending) {
        super(context, c, autoRequery);
        mKeepOnAppending = new AtomicBoolean(keepOnAppending);
        mIsRunInBackground = new AtomicBoolean();
        mContext = context;
    }

    /**
     * Call this method if thread is run
     *
     * @param isRun - flag thread for getting data is run
     */
    public void setRunInBackground(boolean isRun) {
        mIsRunInBackground.set(isRun);
    }

    @Override
    public int getCount() {
        if (mKeepOnAppending.get()) {
            return (super.getCount() + 1); // one more for pending
        }
        return (super.getCount());
    }

    public int getCountForNetworkLoad() {
        return super.getCount();
    }

    /**
     * Use to manually notify the adapter that it's dataset
     * has changed. Will remove the pendingView and update the
     * display.
     */
    public void onDataReady(Cursor cursor) {
        swapCursor(cursor);
    }

    @Override
    public Object getItem(int position) {
        return super.getItem(position);
    }

    public EndlessCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);

        mKeepOnAppending = new AtomicBoolean(false);
    }

    public EndlessCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mKeepOnAppending = new AtomicBoolean(false);
    }


    public void setOnEndlessLoaderListener(OnEndlessLoaderListener  onEndlessLoaderListener){
        this.onEndlessLoaderListener = onEndlessLoaderListener;
    }

    public OnEndlessLoaderListener getOnEndlessLoaderListener(){
        return  this.onEndlessLoaderListener;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // mDataValid variable is private but I need override this method hope it will be OK))
        if (position == super.getCount() && mKeepOnAppending.get()) {
            Cursor c = getCursor();
            mPendingView = newDataView(mContext, c, parent, true);
            if (!mIsRunInBackground.get()) {
                executeAsyncTask();
            } else {
                try {
                    setKeepOnAppending(nextDataPage());
                } catch (Exception e) {
                    setKeepOnAppending(onException(e));
                }
            }
            bindDataView(mPendingView, mContext, getCursor(), true);
            return mPendingView;
        }

        if (!getCursor().moveToPosition(position)) {
            throw new IllegalStateException("couldn't move cursor to position " + position);
        }

        View v;

        if (convertView == null) {
            v = newDataView(mContext, getCursor(), parent, false);
        } else {
            v = convertView;
        }

        bindDataView(v, mContext, getCursor(), false);

        return v;
    }

    /* Don't use- use newDataView instead. */
    @Override
    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        throw new RuntimeException("don't use this method");
    }

    /* Don't use- use bindDataView instead. */
    @Override
    public final void bindView(View view, Context context, Cursor cursor) {
        throw new RuntimeException("don't use this method");
    }

    protected boolean onException(Exception e) {
        Log.e("EndlessAdapter", "Exception in cacheInBackground()", e);
        return (false);
    }

    private Runnable runOnMain = new Runnable() {
        @Override
        public void run() {
            if (getCursor() != null)
                swapCursor(getCursor());
        }
    };

    private Handler mainHandler;

    private void setKeepOnAppending(boolean newValue) {
        boolean same = (newValue == mKeepOnAppending.get());
        if (null == mainHandler)
            mainHandler = new Handler(mContext.getMainLooper());
        mKeepOnAppending.set(newValue);
        if (!same) {
            // save item position in listview123
            mainHandler.post(runOnMain);
        }
    }

    /**
     * Call this method in onDestroyView in UI.
     * This method is closing cursor and shutdown thread pool.
     */
    public void onDestroy() {
        mIsRunInBackground.set(true);
        mPendingView = null;
        if (mainHandler != null) {
            mainHandler.removeCallbacks(runOnMain);
        }
        if (getCursor() != null && !getCursor().isClosed()) {
            getCursor().close();
        }

        if (mExecutorService != null) {
            mExecutorService.shutdown();
        }
    }

    /**
     * save cursor position because dataset is changed
     */
    public abstract void notifyBeforeUpdate();

    /**
     * @param view          - item view in mListView
     * @param context       - context
     * @param cursor        - cursor with data
     * @param isPendingView - show pending view if variable is true.
     * You DON'T get data from cursor if isPendingView because it's end of cursor.
     */
    public abstract void bindDataView(View view, Context context, Cursor cursor, boolean isPendingView);

    /**
     * See {@link #bindDataView(android.view.View, android.content.Context, android.database.Cursor, boolean)}.
     */
    public abstract View newDataView(Context context, Cursor cursor, ViewGroup viewGroup, boolean isPendingView);

    protected abstract boolean nextDataPage() throws Exception;

    private void executeAsyncTask() {
        if (mExecutorService != null) {
            mExecutorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        setKeepOnAppending(nextDataPage());
                    } catch (Exception e) {
                        Log.e("Load some info in Endless cursor adapter:", e.toString());
                        setKeepOnAppending(onException(e));
                    }
                }
            });
        } else {
            try {
                setKeepOnAppending(nextDataPage());
            } catch (Exception e) {
                Log.e("Load some info in Endless cursor adapter:", e.toString());
                setKeepOnAppending(onException(e));
            }
        }
    }

   public interface OnEndlessLoaderListener{
       public  void onNeedShow();
       public void onNeedHide();
   }
}
