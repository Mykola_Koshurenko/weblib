package com.trinetix.webrobo.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.trinetix.webrobo.controllers.BaseRequestController;

/**
 * Created by mykola on 25.09.14.
 */
public abstract class BaseAbstarctFragment extends Fragment {

    private boolean isFirst;
    protected BaseRequestController mRequestController;

    public abstract BaseRequestController createRequestController();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null == mRequestController) {
            mRequestController = createRequestController();
        }
        isFirst = null == savedInstanceState;
        loadData(savedInstanceState, isFirst);
    }

    @Override
    public void onResume() {
        super.onResume();
        initView(isFirst);
    }


    protected abstract void loadData(Bundle bundle, boolean isFirst);

    protected abstract void initView(boolean isFirst);

}
