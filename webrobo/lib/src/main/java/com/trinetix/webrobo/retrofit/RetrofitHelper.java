package com.trinetix.webrobo.retrofit;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.internal.bind.DateTypeAdapter;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.trinetix.webrobo.exceptions.NetworkErrorHandler;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * An aux class to facilitate Retrofit usage.
 * <p/>
 * Created by NK on 25.06.14.
 */
public class RetrofitHelper {
    private static final String CACHE_DIR = "HttpCache";
    private static final String TAG = "RetrofitHelper";

    public static RestAdapter.Builder createApiRestAdapterBuilder(RestAdapter.Builder builder, Context context,
                                                                  Map<Class<?>, JsonDeserializer<?>> mapClassToDeserializer) {
        return createBaseRestAdapterBuilderWithDeserializer(builder, context, mapClassToDeserializer)
                .setErrorHandler(new NetworkErrorHandler());
    }

    public static RestAdapter.Builder createBaseRestAdapterBuilder(RestAdapter.Builder builder, Context context) {
        return createBaseRestAdapterBuilderWithDeserializer(builder, context, null);
    }


    public static RestAdapter.Builder createBaseRestAdapterBuilderWithDeserializer(RestAdapter.Builder builder, Context context, Map<Class<?>, JsonDeserializer<?>> mapClassToDeserializer) {

        if (builder == null) {
            builder = new RestAdapter.Builder();
        }

        // big hack
        GsonBuilder gsonBuilder = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateTypeAdapter());

        // adding custom deserializers for whatever
        if (mapClassToDeserializer != null) {
            for (Class<?> clazz : mapClassToDeserializer.keySet()) {
                gsonBuilder.registerTypeAdapter(clazz, mapClassToDeserializer.get(clazz));
            }
        }

        Gson gson = gsonBuilder.create();

        // providing cache
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        File cacheDirectory = new File(context.getCacheDir().getAbsolutePath(), CACHE_DIR);
        Cache cache = null;
        try {
            cache = new Cache(cacheDirectory, cacheSize);
        } catch (IOException e) {
            Log.e(TAG, "Can't create cache dir");
            e.printStackTrace();
        }

        //create OkHttpClient
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setCache(cache);

        // telling Retrofit to use OkHttpClient
        builder.setClient(new OkClient(okHttpClient));

        // setting the endpoint URL
        builder.setConverter(new GsonConverter(gson))
                .build();

        // enabling logging
        builder.setLogLevel(RestAdapter.LogLevel.FULL);

        return builder.setErrorHandler(new NetworkErrorHandler());
    }

}
