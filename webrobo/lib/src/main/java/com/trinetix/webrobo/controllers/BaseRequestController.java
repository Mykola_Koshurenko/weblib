package com.trinetix.webrobo.controllers;

import android.content.Context;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.trinetix.webrobo.request.Request;
import com.trinetix.webrobo.response.BaseResponse;
import com.trinetix.webrobo.util.Utils;

import java.util.List;

/**
 * Created by mykola on 25.09.14.
 */
public class BaseRequestController {

    private RequestController mRequestController;
    private SpiceManager mSpiceManager;

    public BaseRequestController(SpiceManager spiceManager) {
        mRequestController = new RequestController();
        mSpiceManager = spiceManager;
        if (null == mSpiceManager) throw new NullPointerException("Spice Manager is null");
    }


    private void putRequest(Request request) {
        mRequestController.initRequest(request);
    }

    public <T extends BaseResponse> void exec(final Request request, Context context) {
        putRequest(request);
        if (!Utils.isOnline(context)) {
            request.getListener().onInternetErrorConnection();
            mRequestController.putError(request);
            return;
        }

        mSpiceManager.execute(request.getRequest(), request.getRequest().getKey(), request.getTime(), new RequestListener<T>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                request.getListener().onRequestFailure(spiceException);
                mRequestController.putError(request);
            }

            @Override
            public void onRequestSuccess(T o) {
                if (null != o && null != o.getError()) {
                    request.getListener().onRequestServerError(o.getError());
                } else {
                    request.getListener().onRequestSuccess(o);
                }
                mRequestController.closeRequest(request.getRequest().getKey());
            }
        });
    }

    public void execAll(List<Request> requests, Context context) {
        for (Request request : requests) {
            exec(request, context);
        }
    }


    public void retryFailureRequests(Context context) {
        while (true) {
            Request request = mRequestController.getErrorRequest();
            if (null == request) return;
            exec(request, context);
        }
    }

    public void onStop() {
        mRequestController.clearErrorStack();
        mRequestController.closeRequests();
    }


  /*  public static class RequestConfig{

    }
*/
   /* public static class Builder {
        private Request.RequestBuilder builder;
        public Builder() {
            super();
            builder = new Request.RequestBuilder();
        }

        public Builder request(BaseRequest request) throws NullPointerException {
            builder.request(request);
            return this;
        }

        public Builder time(long time) {
            builder.time(time);
            return this;
        }

        public Builder requestListener(CustomRequestListener listener) {
            builder.requestListener(listener);
            return this;
        }

        public Builder context(Context context) {
            mContext = context;
            return this;
        }


        public Builder spiceManager(SpiceManager spiceManager) {
            if(null==spiceManager){
                throw new NullPointerException("SpiceManager is null");
            }
            mSpiceManager = spiceManager;
            return this;
        }

        public Request build() {
            return builder.build();
        }
    }*/
}
