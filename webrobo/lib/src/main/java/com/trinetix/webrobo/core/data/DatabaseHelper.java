package com.trinetix.webrobo.core.data;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.octo.android.robospice.persistence.ormlite.RoboSpiceDatabaseHelper;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;

import roboguice.util.temp.Ln;

/**
 * A helper for all DAO classes.
 * <p/>
 * Created by NK on 07.07.14.
 */

public abstract class DatabaseHelper extends RoboSpiceDatabaseHelper {
    private static final String TAG = "DatabaseHelper";

    public DatabaseHelper(Context context, SQLiteDatabase.CursorFactory cursorFactory, String dataBaseName, int version) {
        super(context, dataBaseName, cursorFactory, version);
    }

    public abstract void initHelper(Context context);

    public synchronized void releaseHelper() {
        OpenHelperManager.releaseHelper();
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            create(connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error creating DB " + getDatabaseName());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
        upgrade(sqLiteDatabase, connectionSource, oldVer, newVer);
    }

    public abstract void upgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVer, int newVer);

    //protected abstract void clearTables() throws SQLException;

    protected abstract void create(ConnectionSource connectionSource) throws SQLException;

   /* public DaoImpl getDao() throws SQLException {
        if (mDao == null) {
            mDao = new DaoImpl(getConnectionSource(), Object.class);
        }
        return mDao;
    }
*/

    public Dao getDao(Class clazz) {
        try {
            return DaoManager.createDao(getConnectionSource(), clazz);
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }


    @Override
    public <T> void clearTableFromDataBase(Class<T> modelObjectClass) throws SQLException {
        try {
            myClearTable(modelObjectClass);
        } catch (Exception ex) {
            Ln.d(ex, "An exception occurred when cleaning table");
        }
    }


    private <T> void myClearTable(Class<T> clazz) throws SQLException {
        for (Field field : clazz.getDeclaredFields()) {
            ForeignCollectionField annotation = field.getAnnotation(ForeignCollectionField.class);
            if (annotation != null) {
                ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
                Class<?> itemInListClass = (Class<?>) stringListType.getActualTypeArguments()[0];
                myClearTable(itemInListClass);
            }
        }
        TableUtils.clearTable(getConnectionSource(), clazz);
    }
}
