package com.trinetix.webrobo.exceptions;

/**
 * Created by NK on 26.06.14.
 */
public class DeveloperErrorException extends ServerErrorException {

    public DeveloperErrorException(String errorCode, String errorDetails,int httpCode) {
        super(errorCode, errorDetails, httpCode);
    }
}