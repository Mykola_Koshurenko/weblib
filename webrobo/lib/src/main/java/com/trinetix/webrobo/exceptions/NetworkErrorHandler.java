package com.trinetix.webrobo.exceptions;

import android.util.Log;
import com.octo.android.robospice.exception.NoNetworkException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.List;

/**
 * Created by NK on 25.06.14.
 */
public class NetworkErrorHandler implements ErrorHandler {
    private static final String TAG = NetworkErrorHandler.class.getSimpleName();

    @Override
    public Throwable handleError(RetrofitError retrofitError) {
        if (retrofitError.isNetworkError()) {
            Log.w(TAG, "Cannot connect to " + retrofitError.getUrl());
            return new NoNetworkException();
        }

        Response response = retrofitError.getResponse();
        if (response != null) {
            int status = response.getStatus();
            if (status >= 300) {
                Log.w(TAG, "Error " + String.valueOf(status) + " while accessing " + retrofitError.getUrl());
                return retrofitError;
            }
        }

        int index = ExceptionUtils.indexOfType(retrofitError, ServerErrorException.class);

        if (index >= 0) {
            List<Throwable> errorList = ExceptionUtils.getThrowableList(retrofitError);
            ServerErrorException serverErrorException = (ServerErrorException) errorList.get(index);
            if (serverErrorException instanceof com.trinetix.webrobo.exceptions.DeveloperErrorException) {
                Log.e(TAG, "Developer error with code" + serverErrorException.getErrorCode(), serverErrorException);
            }
            return serverErrorException;
        }

        return retrofitError;
    }
}
