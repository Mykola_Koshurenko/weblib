package com.trinetix.webrobo.location;

import android.app.Dialog;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.trinetix.webrobo.ui.BaseAbstractActivity;

/**
 * Created by mykola on 22.10.14.
 */
@Deprecated
public class NewLocationManager implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private final static int
            CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private static final int UPDATE_INTERVAL = 20000;
    private static final int FAST_INTERVAL = 10000;
    private static final String TAG = "LocationManager";
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private BaseAbstractActivity mActivity;

    public void onCreate(BaseAbstractActivity activity) {
        if (isGooglePlayServiceAvailable()) {
            mGoogleApiClient = new GoogleApiClient.Builder(activity).
                    addApi(LocationServices.API).
                    addConnectionCallbacks(this).
                    addOnConnectionFailedListener(this).
                    build();
        }
    }

    public void onStart() {
        if (mGoogleApiClient != null) throw new RuntimeException("GoogleApiClient is null");
        mGoogleApiClient.connect();
    }

    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    public void onDestroy() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnectionCallbacksRegistered(this))
                mGoogleApiClient.unregisterConnectionCallbacks(this);
            if (mGoogleApiClient.isConnectionFailedListenerRegistered(this))
                mGoogleApiClient.unregisterConnectionFailedListener(this);
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FAST_INTERVAL);
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    private boolean isGooglePlayServiceAvailable() {
        if (mActivity != null && !mActivity.isFinishing()) {
            return false;
        }
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(mActivity);
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    mActivity,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);
            if (errorDialog != null) {
                ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
                errorDialogFragment.setDialog(errorDialog);
                errorDialogFragment.show(mActivity.getSupportFragmentManager(), TAG);
            }
            return false;
        }
    }

    public static class ErrorDialogFragment extends DialogFragment {
        private Dialog mDialog;

        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
}
