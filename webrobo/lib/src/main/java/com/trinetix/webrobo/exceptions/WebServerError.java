package com.trinetix.webrobo.exceptions;

/**
 * Created by mykola on 08.12.14.
 */
public class WebServerError extends ServerErrorException {
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
