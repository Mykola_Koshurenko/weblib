package com.trinetix.webrobo.endlessadapter;

/**
 * Listener for users of {@link com.trinetix.eda.adapters.endlessadapter.EndlessAdapter}.
 *
 * Created by mykola on 18.07.14.
 */
public interface IEndlessAdapterListener {

    public boolean hasNextPage();
    public void sendRequest();
    public boolean isWorkingInBackground();
}
